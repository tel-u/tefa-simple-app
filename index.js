const configs = require('./configs/configs');
const bodyParser = require('body-parser');
const express = require('express');
const os = require('os');

const appConfig = configs.get('app');

async function main(){
  const router = express();
  router.use(bodyParser.urlencoded({ extended: false}));
  router.use(bodyParser.json());

  router.get('/health', function(req, res) {
    const responseBody = {
      status: 'OK',
      data: {
        hostname: os.hostname(),
        service: appConfig.name,
        port: appConfig.port,
        client: req.headers['user-agent'],
      },
      message: 'application is running properly'
    };

    res.status(200).json(responseBody);
  });

  const server = router.listen(appConfig.port, function() {
    console.info('application is running on port', appConfig.port);
  });

  process.on('SIGTERM', closeServer(server));
  process.on('SIGINT', closeServer(server));
}

function closeServer(server) {
  return function() {
    server.close(function (err) {
      if (err) {
        console.error(err.message);
      }
      console.info('server is gracefully shutdown')
    });
  };
}

main();