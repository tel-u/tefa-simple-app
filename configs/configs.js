const configs = {
  app: {
    name: process.env.APP_NAME,
    port: process.env.APP_PORT,
  }
};

function get(name) {
  return configs[name];
}

module.exports = {
  get: get
}
